var visibleElementArray = []

function calculateAmountOfElementsInView() {
		var windowHeight = $(window).height()
		var currentScrollPosition = $(window).scrollTop()
		var count = 0
		$(".omitted a").each(function (data) {
			var elOffset = $(this).offset().top
			var elHeight = $(this).outerHeight()
			 if (currentScrollPosition > (elOffset+elHeight-windowHeight)
			 && (elOffset > currentScrollPosition)
			 && (windowHeight + currentScrollPosition > elOffset + elHeight)) {
				$(this)[0].click()
			}
		})
}

$(document).ready(function () {

	$(".omitted a").each(function (e) {
		$(this).replaceWith($(this).clone())
	})
	$(".omitted a").each(function (data) {
		var link = $(this).parent().siblings(".intro").children(".post_no").attr("href")
		$(this).attr("href", link)
		$(this).attr("target", "_blank")
		$(this).attr("rel", "noopener noreferrer")
	})
})


$(document).keydown(function(e) {	
	if ( e.which == 69 && e.ctrlKey) {
		 calculateAmountOfElementsInView()
	}
})

