# 8chanExpander

Remaps the "Click to expand" link on 8chan to opening the thread in a new tab. Also remaps Control+E to open all "Click to expand" links currently in view. 

# Comments and Feedback

I gratefully accept all comments and feedback. If you have any, please open an issue here on  GitLab and I will address it as soon as I can.
